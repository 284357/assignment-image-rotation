//
// Created by katya on 11.03.2022.
//

#ifndef ROTATION_H
#define ROTATION_H

struct image rotate(struct image source_image);

#endif //ROTATION_H
