//
// Created by katya on 11.03.2022.
//

#ifndef PADDING_H
#define PADDING_H

#include <stdint.h>

uint8_t get_padding(uint32_t width);

#endif //PADDING_H
